import React, { Component } from 'react';
import _ from 'lodash';

export default class CreateTodo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null
    };
  }

  render() {
    return (
      <form onSubmit={this.handleCreate.bind(this)}>
        <input type="text" placeholder="Titre du livre" ref="createInput"/>
        <input type="text" placeholder="Autheur du livre" ref="createInputAutheur"/>
        <button>Create</button>
        {this.renderError()}
      </form>
    );
  }

  renderError() {
    if(!this.state.error) { return null; }

    return <div style={{color: 'red'}}>{this.state.error}</div>
  }

  handleCreate(event) {
    event.preventDefault();

    const createInput = this.refs.createInput;
    const titre = createInput.value;
    const validateInput= this.validateInput(titre);

    if(validateInput) {
      this.setState({ error: validateInput });
      return;
    }
    
    const createInputAutheur = this.refs.createInputAutheur;
    const autheur = createInputAutheur.value;
    const validateInputAutheur= this.validateInputAutheur(autheur);
    
    this.setState({ error: null });
    this.props.createLivre(titre, autheur);
    this.refs.createInput.value = '';
    this.refs.createInputAutheur.value = '';
  }
  
  // if (validateInput !== null && validateInputAutheur !== null) {
  //   this.setState({ error: validateInput });
  //   return;
  // }

  validateInput(titre) {
    if(!titre) {
      return "Entrez le titre d'un livre";
    } else if (_.find(this.props.livres, livre => livre.titre === titre)) {
      return "Livre existe déjà";
    } else {
      return null;
    }
  }
  
  validateInputAutheur(autheur) {
    if(!autheur) {
      return "Entrez l'autheur du livre";
    } else {
      return null;
    }
  }
}
